/// Place some number of dividers on a width (holes in a beam)
pub fn div(width: f64, div: u32, pad: Option<f64>, off: Option<f64>) -> Result<Vec<f64>, &'static str> {

	let pad = pad.unwrap_or(width.abs() / (div + 1) as f64);
	let off = off.unwrap_or(0.0);

	if pad * 2.0 > width.abs() {
		return Err("more padding than width");
	}
	if (pad * 2.0 == width.abs()) && div > 1 {
		return Err("no room for more than 1 divider");
	}

	if div == 1 {
		return Result::Ok(vec![off + width / 2.0]);
	}
	else {
		let step_size = (width - pad * 2.0) / (if div <= 1 {1} else {div - 1}) as f64;
		let mut divs: Vec<f64> = Vec::new();
		for step in 0..div {
			divs.push(off + pad + (step_size * step as f64));
		}
		Result::Ok(divs)
	}
}


#[test]
/// no dividers on no width
fn null() {

	let correct: Vec<f64> = vec![];

	let result = div(0.0, 0, None, None).expect("");

	assert_eq!(correct, result);
}

#[test]
/// 1 divider right in the middle
fn minimal() {

	let correct = vec![5.0];

	let result = div(10.0, 1, None, None).expect("");

	assert_eq!(correct, result);
}

#[test]
/// 5 dividers on a width 0 padding
fn no_padding() {

	let correct = vec![0.0, 5.0, 10.0, 15.0, 20.0];

	let result = div(20.0, 5, Some(0.0), Some(0.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// 5 dividers on a width with an offset
fn full() {

	let correct = vec![3.0, 7.0, 11.0, 15.0, 19.0];

	let result = div(20.0, 5, Some(2.0), Some(1.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// maximum width divided in 2 (by 1 divider)
fn max() {

	let max = f64::MAX;

	let correct = vec![max / 2.0];

	let result = div(max, 1, None, None).expect("");

	assert_eq!(correct, result);
}

#[test]
/// maximum width with 1 divider, padding of 1 and offset of 2
fn max_padded_offset() {

	let max = f64::MAX;

	let correct = vec![(max / 2.0) + 1.0 + 2.0];

	let result = div(max, 1, Some(1.0), Some(2.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// negative numbers where possible
fn full_negative() {

	let correct: Vec<f64> = vec![-3.0, -7.0, -11.0, -15.0, -19.0];

	let result = div(-20.0, 5, Some(-2.0), Some(-1.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// negative offset
fn negative_offset() {

	let correct: Vec<f64> = vec![4.0];

	let result = div(10.0, 1, None, Some(-1.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// negative offset with centered stuff
fn negative_offset_centered() {

	let correct: Vec<f64> = vec![-5.0, 0.0, 5.0];

	let result = div(10.0, 3, Some(-0.0), Some(-5.0)).expect("");

	assert_eq!(correct, result);
}

#[test]
/// negative offset
fn negative_padding() {

	let correct: Vec<f64> = vec![-1.0, 5.0, 11.0];

	let result = div(10.0, 3, Some(-1.0), Some(-0.0)).expect("");

	assert_eq!(correct, result);
}
