use clap::Parser;

pub mod div;

// example output in logical format
// width: 20, dividers: 5, padding: 2, step: 4, offset: 1
// | offset |                     total width                     |
// | offset | padding ¦ remaining width with n dividers ¦ padding |
// |   1    |   2     ¦3      7       11      15      19¦    2    |

/// Place some number of dividers on a width (holes in a beam)
#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {

	// TODO: accept these as positional arguments, explicit long, medium and short options, and ask for the required arguments if they are not provided

	/// total width to be divided
	//#[clap(short = 'w', long = "width")]
	width: f64,
	/// how many dividers to distribute on the width
	//#[clap(short = 'd', long = "div", long = "dividers")]
	div: u32,
	/// enforce a minimum padding from the ends of width to the first point (defaults to width / div + 1)
	//#[clap(short = 'p', long = "pad", long = "padding")]
	pad: Option<f64>,
	/// offset to the region divided
	//#[clap(short = 'o', long = "off", long = "offset")]
	offset: Option<f64>,
}

/// Place some number of dividers on a width (holes in a beam)
fn div(width: f64, div: u32, pad: Option<f64>, off: Option<f64>) -> Result<Vec<f64>, &'static str> {

	let pad = pad.unwrap_or(width.abs() / (div + 1) as f64);
	let off = off.unwrap_or(0.0);

	if pad * 2.0 > width.abs() {
		return Err("more padding than width");
	}
	if (pad * 2.0 == width.abs()) && div > 1 {
		return Err("no room for more than 1 divider");
	}

	if div == 1 {
		return Result::Ok(vec![off + width / 2.0]);
	}
	else {
		let step_size = (width - pad * 2.0) / (if div <= 1 {1} else {div - 1}) as f64;
		let mut divs: Vec<f64> = Vec::new();
		for step in 0..div {
			divs.push(off + pad + (step_size * step as f64));
		}
		Result::Ok(divs)
	}
}

fn main() {

	let args = Cli::parse();
	let divs = div(args.width, args.div, args.pad, args.offset).expect("");
	println!("\nwidth: {:.3}, dividers: {}, padding: {:.3}, step: {:.3}, offset: {:.3}\n"
		, args.width, args.div
		, args.pad.unwrap_or(args.width / (args.div - 1) as f64)
		, (args.width - args.pad.unwrap_or(args.width / (args.div - 1) as f64) / (args.div - 1) as f64)
		, args.offset.unwrap_or(0.0));

	for div in divs { println!("{:.3}", div); }
}
