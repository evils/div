{pkgs ? import <nixpkgs> {
  overlays = [
    (import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz))
  ];
  }
}:

with pkgs;

stdenv.mkDerivation {
  name = "rustenv";

  nativeBuildInputs = [
    # nix native alternative to rustup to get the toolchain
    # also provides cargo, rustfmt, etc
    pkgs.rustChannels.stable.rust
    # debugger
    gdb
    # needed by cargo
    openssl
  ];

  RUST_BACKTRACE = 1;
  # hint, use lorri shell if you want to keep your shell and prompt
}
