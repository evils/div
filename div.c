#include <stdio.h>

int main(){
	// TODO: accept these as arguments
	// width of board
	float width;
	// number of screws
	float div;
	// distance from edge of board to place first and last screw
	float pad;
	// distance from edge of board where measurement starts
	float off;
	printf("enter width: ");
	scanf("%f", &width);
	printf("\nenter dividers : ");
	scanf("%f", &div);
	printf("\nenter padding: ");
	scanf("%f", &pad);
	printf("\nenter offset: ");
	scanf("%f", &off);

	if (pad * 2 > width){
		printf("cannot place a divider in width %f with padding of %f!\n", width, pad);
		return 1;
	}

	float step;
	if (div == 1) {
		printf("%07.3f\n", off + (width / 2));
	}
	else {
		step = (width - (2 * pad)) / (div - 1);
		printf("\nwidth: %.3f, dividers: %.3f, padding: %.3f, step: %.3f, offset: %.3f\n\n"
			, width, div, pad, step, off);

		for (float f = pad; f <= (width + pad); f += step){
			printf("%07.3f\n", f + off);
		}
	}
	return 0;
}

